object Form1: TForm1
  Left = 236
  Top = 178
  BorderStyle = bsSingle
  Caption = #1052#1086#1088#1089#1082#1086#1081' '#1073#1086#1081
  ClientHeight = 249
  ClientWidth = 566
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnMouseUp = FormMouseUp
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 80
    Top = 0
    Width = 21
    Height = 13
    Caption = '       '
  end
  object Label2: TLabel
    Left = 384
    Top = 0
    Width = 12
    Height = 13
    Caption = '    '
  end
  object Button1: TButton
    Left = 232
    Top = 8
    Width = 75
    Height = 25
    Caption = #1053#1086#1074#1072#1103
    TabOrder = 0
    OnClick = Button1Click
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 230
    Width = 566
    Height = 19
    Panels = <
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 200
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object Button2: TButton
    Left = 232
    Top = 40
    Width = 75
    Height = 25
    Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
    TabOrder = 2
    OnClick = Button2Click
  end
  object MainMenu1: TMainMenu
    Left = 224
    Top = 200
    object N1: TMenuItem
      Caption = '&'#1048#1075#1088#1072
      object N2: TMenuItem
        Caption = #1053#1086#1074#1072#1103' &'#1086#1076#1080#1085#1086#1095#1085#1072#1103
        OnClick = N2Click
      end
      object N3: TMenuItem
        Caption = #1053#1086#1074#1072#1103' &'#1085#1072' '#1086#1076#1085#1086#1084' '#1082#1086#1084#1087#1100#1102#1090#1077#1088#1077'...'
        OnClick = N3Click
      end
      object N4: TMenuItem
        Caption = #1053#1086#1074#1072#1103' (&'#1089#1077#1088#1074#1077#1088')'
        OnClick = N4Click
      end
      object N5: TMenuItem
        Caption = #1053#1086#1074#1072#1103' (&'#1082#1083#1080#1077#1085#1090')...'
        OnClick = N5Click
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object N7: TMenuItem
        Caption = '&'#1042#1099#1093#1086#1076
        OnClick = N7Click
      end
    end
    object N8: TMenuItem
      Caption = '&'#1055#1072#1088#1072#1084#1077#1090#1088#1099
      object N9: TMenuItem
        Caption = '&'#1057#1083#1086#1078#1085#1086#1089#1090#1100'...'
        OnClick = N9Click
      end
      object N13: TMenuItem
        Caption = '&'#1040#1074#1090#1086#1084#1072#1090#1080#1095#1077#1089#1082#1072#1103' '#1088#1072#1089#1089#1090#1072#1085#1086#1074#1082#1072
        Checked = True
        OnClick = N13Click
      end
    end
    object N10: TMenuItem
      Caption = #1055'&'#1086#1084#1086#1097#1100
      object N11: TMenuItem
        Caption = '&'#1057#1087#1088#1072#1074#1082#1072
        OnClick = N11Click
      end
      object N12: TMenuItem
        Caption = '&'#1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
        OnClick = N12Click
      end
    end
  end
  object ClientSocket1: TClientSocket
    Active = False
    ClientType = ctNonBlocking
    Port = 1024
    OnConnect = ClientSocket1Connect
    OnDisconnect = ClientSocket1Disconnect
    OnRead = ClientSocket1Read
    OnError = ClientSocket1Error
    Left = 256
    Top = 200
  end
  object ServerSocket1: TServerSocket
    Active = False
    Port = 1024
    ServerType = stNonBlocking
    OnClientConnect = ServerSocket1ClientConnect
    OnClientDisconnect = ServerSocket1ClientDisconnect
    OnClientRead = ServerSocket1ClientRead
    OnClientError = ServerSocket1ClientError
    Left = 288
    Top = 200
  end
end
